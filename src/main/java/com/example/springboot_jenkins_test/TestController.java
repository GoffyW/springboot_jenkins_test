package com.example.springboot_jenkins_test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Author: GuoFei
 * @Date: 2021/06/05/14:41
 * @Description:
 */
@RestController
@RequestMapping("test")
public class TestController {
    @GetMapping("get/{id}")
    public String test(@PathVariable("id") Integer id){
        return "返回id="+id;
    }

}
